import Vue from "vue";
import VueRouter from "vue-router";

import store from "../store";

Vue.use(VueRouter);

const routes = [
	{
		path: "/",
		name: "Home",
		component: () => import("../views/Home.vue"),
		meta: {
			administrador: true,
			almacenero: true,
			vendedor: true
		}
	},
	{
		path: "/about",
		name: "About",
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/About.vue"),
		meta: {
			administrador: true,
			almacenero: true,
			vendedor: true
		}
	},
	{
		path: "/categorias",
		name: "Categoria",
		component: () => import("../views/Categoria.vue"),
		meta: {
			administrador: true,
			almacenero: true,
		}
	},
	{
		path: "/articulos",
		name: "Articulo",
		component: () => import("../views/Articulo.vue"),
		meta: {
			administrador: true,
			almacenero: true,
		}
	},
	{
		path: "/roles",
		name: "Rol",
		component: () => import("../views/Rol.vue"),
		meta: {
			administrador: true,
		}
	},
	{
		path: "/usuarios",
		name: "Usuario",
		component: () => import("../views/Usuario.vue"),
		meta: {
			administrador: true,
		}
	},
	{
		path: "/clientes",
		name: "Cliente",
		component: () => import("../views/Cliente.vue"),
		meta: {
			administrador: true,
			vendedor: true
		}
	},
	{
		path: "/proveedores",
		name: "Proveedor",
		component: () => import("../views/Proveedor.vue"),
		meta: {
			administrador: true,
			almacenero: true,
		}
	},
	{
		path: "/login",
		name: "Login",
		component: () => import("../views/Login.vue"),
		meta: {
			libre: true
		}
	},
	{
		path: "/ingresos",
		name: "Ingreso",
		component: () => import("../views/Ingreso.vue"),
		meta: {
			administrador: true,
			almacenero: true
		}
	},
	{
		path: "/ventas",
		name: "Venta",
		component: () => import("../views/Venta.vue"),
		meta: {
			administrador: true,
			vendedor: true
		}
	},
	{
		path: "/consultaventa",
		name: "ConsultaVenta",
		component: () => import("../views/ConsultaVenta.vue"),
		meta: {
			administrador: true
		}
	},
	{
		path: "/consultaingreso",
		name: "ConsultaIngreso",
		component: () => import("../views/ConsultaIngreso.vue"),
		meta: {
			administrador: true
		}
	}
];


const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes
});

router.beforeEach((to, from, next) => {
	if (to.matched.some(record => record.meta.libre)){
        next()
    }else if(store.state.usuario && store.state.usuario.rol == 'Administrador'){
        if (to.matched.some(record => record.meta.administrador)){
            next()
        }
    }else if(store.state.usuario && store.state.usuario.rol == 'Almacenero'){
        if (to.matched.some(record => record.meta.almacenero)){
            next()
        }
    }else if(store.state.usuario && store.state.usuario.rol == 'Vendedor'){
        if (to.matched.some(record => record.meta.vendedor)){
            next()
        }
    } else{
        next({
            name:'Login'
        })
    }
})



export default router;
